To list extensions:  
```code --list-extensions > extensions.txt```

To install extensions:  
* Unix  
```cat extensions.txt | xargs -L 1 echo code --install-extension```    
* Windows  
```cat extensions.txt | % { "code --install-extension $_" }```
